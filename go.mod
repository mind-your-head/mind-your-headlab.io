module gitlab.com/mind-your-head/mind-your-head.gitlab.io

// For local development, assuming the theme dir is at the same level as the site dir:
// uncomment the line below:
// replace gitlab.com/lsrdg/cirkus => ../cirkus

go 1.16

require (
	github.com/theNewDynamic/gohugo-theme-ananke v0.0.0-20220907123632-5a8b531a7ce2 // indirect
)
