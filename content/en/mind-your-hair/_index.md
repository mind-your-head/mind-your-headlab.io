---
title: ' 🔥 Mind Your Hair'
description: 'The fire crew. Fire safety, juggling jams, fire workshops, fire shows, fire safety...'
menu:
  main:
    weight: 5
translationKey: 'mindyourhair'
---

# Respect for the Fire

Juggling and fire has always been a thing. However, it must be done right, or at least not wrongly.
**Mind Your Hair** is a group in Mind Your Head responsible for all things fire-related.

The group gets busy with:

- 🔥Fire safety
- 🔥Fire workshops
- 🔥Fire safety
- 🔥Fire jams
- 🔥Fire safety

> If you would like to get started with fire juggling, get in touch, and don't do it alone, find more experienced people to talk to!

Here are our basic guidelines for fire, just in case anyone is in doubt. You can download it as well:

- [myh-fire-safety-guide.pdf](myh-fire-safety-guide.pdf)

## Mind your head fire safety

### General principles:

- Never spin fire alone, always have people with you for safety
- Always wear non-flammable material such as cotton, wool, linen or similar, plastics and polyester must be taken off before anything gets lit on fire
- Always have a wet towel ready for extinguishing props
- Always have a “brandfilt” if anything catches fire that needs to be extinguished, including people
- Have clearly defined and separated stations for dipping, storing dipped props, lighting up, extinguishing, and cooling down
- Have remedies at the ready if people get burnt, cooling creams etc.
- Always have at least one person per 3 or 4 fire spinners, watching over the fire space
- Do what you are comfortable with and have practised and feel safe with
- Always be mindful of where you chose to spin in regard to fire hazards
  - Good locations are open planes, hills, grasslands, beaches, parking lots or in general outdoors with a lot of space
- Bringing new people in to trying fire should be done with consent and extra care regarding safety
- Respect authorities and fire rules, local, general and temporal (fire bans due to weather etc.)
- Be mindful of how many times you dipp, oil is almost always a scarce resource and people should get the chance to join
- Be mindful of the person being on fire safety duty, switch regularly so they can join the fun

### Dipping station

- A clearly marked jug or cut piece of PET-bottle with oil, preferably with some light source nearby so it won’t get knocked over
- Space around to “skvätta av” the props so oil won’t come flying when lit on fire
- A dry towel to squeeze of excess oil on props that need it
- No fire source close to this station

### Storing station

- Any piece of fabric or towel that the props can lie on once dipped, this helps to clear up the confusion of what is and isn’t dipped
- Alternatively put them on the ground, but clearly separated from what’s already been dipped

### Lighting station

- A clear location with a source of fire, preferably a “marshall”
- Should be fairly close to the spinning area so as to not walk through sensitive equipment or people with lit props

### Extinguishing station

- A wet towel for extinguishing props
- A “brandfilt” for extinguishing objects and people
- Fire safety person should always sit ready by this station

### Cooling down station

- Close to the extinguishing station
