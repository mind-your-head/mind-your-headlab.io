---
title: 'Mind Your Head'
featured_image: '/images/myh.jpg'
description: "+3 decades old juggling association. Copenhagen's juggling club based on Nørrebro"
tabs:
services:
  - name: 'Mind WHAT?!'
    text: "A juggling club in Copenhagen: for EVERYONE. We facilitate a practicing space where jugglers (despite age, gender, color, language or props) can meet each other. A great place for beginners, professionals, hobbyists etc. Let's learn from each other!"
    buttonText: 'Read More'
    url: 'about'
  - name: 'Where?'
    text: 'TUESDAY from 19:45 to 23:00 at Støberiet. 3rd floor at Blågårds Plads 5, 2200 København N.'
    text2: 'Thursday from 19:30 to 22:00 at Den Classenske Legatskole. Vester Voldgade 98, 1552 København.'
  - name: 'Contact US'
    text: 'Throw a diabolo higher than 147m and jugglers from all over the country will join you.'
---

...for EVERYONE. We facilitate a practicing space where jugglers (despite age, gender, color, language or props) can meet each other. A great place for beginners, professionals, hobbyists etc. Let's learn from each other!
