---
title: 'Fire workshop with Vitek'
featured_image: 'images/news/vitek.jpg'
date: 2022-10-04
description: 'Fireworkshop: the great Vitek is coming to share his experiences with us!'
---

🔥 In Mind Your Hair's 3rd fire workshop (in 2022) we are glad to introduce flowartist and fire performer Vitek as our host.

🔥 The workshop will be held on Blågårds Plads Tuesday 4th October from 8:00pm.

🔥 All our workshops are public and free for all, regardless of knowledge and skill level.

🔥 Bring your own fire props if you got any.

Here is some more information from the workshop's host:

```
Hello
My name is Vitek and I would like to invite You for Tuesday flow art meeting!
This time I will have a pleasure to lead little fire workshop 🔥😁
Something about me - I’m fire performer with 5 years of experience. My specialisations are POI, DoubleStaffs, spinning staff and fire breeze 🔥🔥🔥
But I do love all flow props!
You can expect to get to know how I prepare for the show, see my drill of fire safety and hear about my view on stage performing with Fire 🔥.
After my little introduction we shall have a FIRE JAM 🔥
So don’t forget to bring your fire toys 😜
See You soon 😉
```
