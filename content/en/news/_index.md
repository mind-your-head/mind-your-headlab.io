---
title: 'News'
menu:
  main:
    weight: 3
translationKey: 'news'
---

Here you can keep up to date with what's going on in MYH (without using social media). If you insist, find us on [facebook](https://www.facebook.com/groups/135589039850555/)
