---
title: Contact
featured_image: ''
omit_header_text: true
description: We'd love to hear from you
menu:
  main:
    weight: 4
translationKey: 'contact'
---

And here we can have a nice text telling people that there is enough metaphysics in not thinking about nothing. And maybe tell them our e-mail address as well.

{{< form-contact action="https://example.com"  >}}
