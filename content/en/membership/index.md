---
title: 'Membership'
description: 'Become a member of a memberless crew'
menu:
  main:
    weight: 2
translationKey: 'membership'
---

Membership? Costs DKK 120 per year and can be paid to:

reg. 2120
account. 3495517275
bank: Nordea

Write an email to myh.kbh@gmail.com and say that you have paid
